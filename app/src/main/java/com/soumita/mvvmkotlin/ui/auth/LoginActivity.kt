package com.soumita.mvvmkotlin.ui.auth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.soumita.mvvmkotlin.R
import com.soumita.mvvmkotlin.databinding.ActivityLoginBinding
import com.soumita.mvvmkotlin.util.hide
import com.soumita.mvvmkotlin.util.show
import com.soumita.mvvmkotlin.util.toast
import kotlinx.android.synthetic.main.activity_login.*
import kotlin.math.log

class LoginActivity : AppCompatActivity(), AuthListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_login)

        val loginActivityBinding : ActivityLoginBinding = DataBindingUtil.setContentView(this,R.layout.activity_login)

        val viewmodel = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        loginActivityBinding.viewmodel = viewmodel

        viewmodel.authListener = this
    }

    override fun onStarted() {
//        toast("login Started")
        progress_bar.show()
    }

    override fun onSuccess(loginResponse: LiveData<String>) {
        loginResponse.observe(this, Observer {
            progress_bar.hide()
            toast(it)
        })
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        toast("login Failure")
    }
}
