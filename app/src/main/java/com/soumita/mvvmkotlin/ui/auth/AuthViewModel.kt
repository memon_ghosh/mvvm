package com.soumita.mvvmkotlin.ui.auth

import android.view.View
import androidx.lifecycle.ViewModel
import com.soumita.mvvmkotlin.data.repositories.UserRepository

class AuthViewModel : ViewModel() {


    var email: String? = null
    var password: String? = null

    var authListener: AuthListener? = null

    fun onLoginButtonClick(view: View) {
        authListener?.onStarted()
        if (email.isNullOrEmpty() || password.isNullOrEmpty()) {
            //show error message
            authListener?.onFailure("Invalid email or password")
        }else {
            //success message
            val loginResponse = UserRepository().userLoginApiCall(email!!, password!!)
            authListener?.onSuccess(loginResponse)
        }
    }
}